const tabs = [
  {
    title: 'Overview',
    tab: 'main'
  },
  {
    title: 'Screening',
    tab: 'screening',
    notMatched: true
  },
  {
    title: 'Proof of Address',
    tab: 'proof'
  },
]
module.exports = {
  tabs,
  current: tabs[1].tab
};
